;;; srfi-23-el --- SRFI-23 for Emacs                 -*- lexical-binding: t -*-
;;; Copyright (C) Stephan Houben (2001). All Rights Reserved.
;;; Copyright © 2017 Alex Vong

;;; Permission is hereby granted, free of charge, to any person obtaining
;;; a copy of this software and associated documentation files (the
;;; "Software"), to deal in the Software without restriction, including
;;; without limitation the rights to use, copy, modify, merge, publish,
;;; distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to
;;; the following conditions:

;;; The above copyright notice and this permission notice shall be included
;;; in all copies or substantial portions of the Software.

;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


(require 'r5rs)


(r5-define (s23-error reason &rest args)
  (let* ((spaces (make-list (length args) " "))
         (args* (r5-map #'prin1-to-string args))
         (spaces-args*-interleaved (apply #'append
                                          (r5-map #'list spaces args*))))
    (signal 'error
            (list (apply #'r5-string-append
                         reason
                         spaces-args*-interleaved)))))


(provide 'srfi-23)
